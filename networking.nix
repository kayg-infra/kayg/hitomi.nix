{ config, lib, pkgs, ... }:

{
  networking.hostName = "hitomi";
  networking.firewall.enable = true;

  /* ipv4 section */
  networking.interfaces.ens18.ipv4.addresses = [ {
      address = "173.249.59.225";
      prefixLength = 19;
    }
  ];

  networking.defaultGateway = {
    address = "173.249.59.1";
    interface = "ens18";
  };

  /* ipv6 section */
  networking.interfaces.ens18.ipv6.addresses = [ {
      address = "2a02:c207:2048:6231:0000:0000:0000:0001";
      prefixLength = 64;
    }
  ];

  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "ens18";
  };

  /* Linux has something called AnyIP which allows a subnet to be bound to an
  interface instead of a single IP address. This is particularly useful for
  ipv6, in which case providers assign /64 blocks a single server, allowing the
  user to choose between 16^16 addresses. It would become rather cumbersome to
  assign addresses one by one.

  The loopback interface already makes use of Linux's AnyIP.
  For example: ip addr show lo
  will output this: inet 127.0.0.1/8 scope host lo
  which basically says that all IPs from 127.0.0.1 to 127.0.0.255 belong to that host.

  Similarly, adding a subnet as a route to the loopback interface will bind the
  whole subnet to the host.

  The implementation here: https://github.com/NixOS/nixpkgs/blob/release-20.03/nixos/modules/tasks/network-interfaces-scripted.nix#L214
  doesn't allow for the following to work as there's no ${var} between `ip route add` and ${cidr}.

  networking.interfaces.lo.ipv6.routes = [ {
      address = "2a02:c207:2038:2340::1";
      prefixLength = 64;
      options = {
        to = "local";
      };
    }
  ];

  For now, this will have to do:
  networking.localCommands = ''
    ip route add local 2a02:c207:2038:2340::1/64 dev lo
  ''; */
}
