{ config, lib, pkgs, ... }:

{
  users = {
    users = {
      # Define user accounts
      kayg = {
        createHome = true;
        extraGroups = [ "wheel" "docker" ]; # Enable ‘sudo’ for the user.
        hashedPassword = "$6$VNu7UUuVDBhWk$PQ8X0kjFnDnpUs59oNpnx3Vb3IwYPsU9JhyZoQLL9BVItc4BLngE2gLvNWlsjvu9/jGNgnwDZIEpdik58K/0K0";
        home = "/home/kayg";
        isNormalUser = true;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILJHksfJBFHXoZbu2QU0OWPDQlsHvhFtOiNaO3KFzBgy kayg@nana"
        ];
        shell = pkgs.zsh;
        uid = 1000;
      };
    };
  };
}
