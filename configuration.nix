{ config, lib, pkgs, ... }:

{
  imports = [
    # include results of the hardware scan
    ./hardware-configuration.nix
    # include commonly used options
    ./common/system/boot.nix
    ./common/system/common.nix
    ./common/system/docker.nix
    ./common/system/encryption.nix
    ./common/system/environment.nix
    ./common/system/networking.nix
    # include locally used options
    ./boot.nix
    ./environment.nix
    ./filesystems.nix
    ./networking.nix
    ./services.nix
    ./users.nix
    # include home-manager
    <home-manager/nixos>
  ];

  # contabo VPS HDD line-up is all intel
  hardware.cpu.intel.updateMicrocode = true;

  home-manager.users.kayg = import ./home.nix;

  programs.fuse.userAllowOther = true;

  time.timeZone = "Europe/Berlin";
}
