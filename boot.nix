{ config, lib, pkgs, ... }:

{
  boot.loader.grub = {
    efiSupport = false;
    device = "/dev/sda";
  };

  boot.initrd.availableKernelModules = [ "virtio_pci" ];

  boot.initrd.network.postCommands = "
    echo 'cryptsetup-askpass' >> /root/.profile
  ";

  boot.initrd.network.ssh.authorizedKeys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEQUNogYPRHACEmEuK1Q6ZRIJEIq5vxnRQM9yHAO37vV kayg@nana"
  ];

  # use hardened kernel
  boot.kernelPackages = pkgs.linuxPackages_latest_hardened;
}
