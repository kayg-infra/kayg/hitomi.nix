{ config, lib, pkgs, ... }:


{
  fileSystems."/".options = [
    "defaults"
    "noatime"
    "nobarrier"
  ];

  fileSystems."/boot".options = [
    "defaults"
    "noatime"
    "nobarrier"
  ];
}
