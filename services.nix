{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in

{
  /* Borg backup repositories */
  /* Making multiple repositories poses a problem with the rclone mount because:
  1. rclone runs as an unprivileged user
  2. borg runs as root

  So when the rclone mount breaks for some reason or even in normal cases with a
  `nixos-rebuild switch`; for the brief amount of time between the unmount and
  the remount, borg sees that the repo directories inside /backups do not exist
  and hence proceeds to create them. Since borg runs as root, it has no issues
  with overriding permissions.

  I figured I might as well go with one fat borg repo with subrepos, which
  allows flexibility, instead of different repos with subrepos. */
  services.borgbackup.repos.backups = {
    allowSubRepos = true;
    authorizedKeys = [
      /* kayg */
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINk27uX6mEPiQfIpg32wIalJLM63mEMqFef7YHuYvUvy root@anri"
      /* sasach */
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKtM+z2/2HSD+Wqa0BVgR7E5T08X7AhvinCDKVmTMYua sasach@work"
      /* pandacowbat */
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAQn2NzWn/ldJU91rNdy3TSisPOhkZapyPRx06lw+ZR9 root@vmi281403.contaboserver.net"
    ];
    /* all files in this directory must be owned by the user 'borg' */
    path = /backups;
  };

  /* Mount drive for backups */
  systemd.services.rclone-mount-backups = {
    after = [
      "network-online.target"
    ];
    description = "Mount encrypted drive at /backups";
    environment = {
      RCLONE_CONFIG= "/home/kayg/.config/rclone/rclone.conf";
    };
    path = [
      "/run/wrappers"
      unstable.rclone
    ];
    restartIfChanged = true;
    serviceConfig = {
      ExecStart = ''
     ${unstable.rclone}/bin/rclone mount drive-encrypted:/backups /backups \
       --allow-other \
       --buffer-size 256M \
       --dir-cache-time 1000h \
       --umask 002 \
       --vfs-cache-mode writes
    '';
      ExecStop = ''
     /run/wrappers/bin/fusermount -uz /backups
    '';
      Group = "users";
      KillMode = "none";
      Restart = "on-failure";
      RestartSec = 5;
      Type = "notify";
      User = "kayg";
    };
    wantedBy = [
      "multi-user.target"
    ];
  };
}
